package learnproject;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class IMDbScraper {
    public static void main(String[] args) {
        try {
            // Connect to IMDb top rated movies page
            Document doc = Jsoup.connect("https://www.imdb.com/chart/top/?ref_=nv_mv_250").get();

            // Select all movie elements
            Elements movies = doc.select(".ipc-metadata-list-summary-item__c");
            int a = 0;
            for (Element movie : movies) {
                // Extract title
            	
                // Iterate over each movie element class="ipc-title__text"
                Element titleElement = movie.select(".ipc-title__text").first();
                String title = titleElement.text();
                Elements ratingAndViewsElements = doc.getElementsByClass("ipc-rating-star ipc-rating-star--base ipc-rating-star--imdb ratingGroup--imdb-rating");

                Element ratingAndViews = ratingAndViewsElements.get(a);
                String ratAndView = ratingAndViews.text();
                String[] rating = ratAndView.split(" ");
                
                                        
                System.out.println(title + " - Rating " + rating[0] + " - Views " + rating[1]);
                a++;
                
                if(a>=10){
                	break;
                }
                
            }
                 
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
