package learnproject;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;

public class SchoolProject {



 public static void main(String[] args) {
  int[] marks = new int[5];
  
  SchoolProject sp = new SchoolProject();
  sp.getInput(marks);
  int total = sp.getTotal(marks);
  System.out.println("Total : "+total);
  int avg = sp.getAvg(total,marks.length);
  System.out.println("AVG : "+avg);
  String result = sp.getGrade(avg);
  System.out.println(result);

  
 }

 private String getGrade(int avg) {
  String grade="";
  if(avg>=90) {
   grade="A";
  }else if(avg>=80) {
   grade="B";
  }else if(avg>=70) {
   grade="C";
  }else {
   grade="D";
  }
  return grade;
 }

 private int getAvg(int total,int noOfSub) { 
  int avg = total/noOfSub;
  return avg;
  
 }

 private int getTotal(int[] marks) {
  int total=0;
  for(int i=0;i<marks.length;i++) {
   total = total + marks[i];
  }
  return total;
 }

 private void getInput(int[] marks) {
  Scanner sc = new Scanner(System.in); 
  for(int i=0;i<marks.length;i++) {
   System.out.println("Enter marks: ");
   marks[i]=sc.nextInt();
  }
  
 }

}
