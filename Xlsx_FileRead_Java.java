package learnproject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Xlsx_FileRead_Java {

	public static void main(String[] args) throws IOException {

		try {
			File file = new File("/home/ramu/Downloads/Chennai batting(1).xlsx");

			FileInputStream input = new FileInputStream(file);
			XSSFWorkbook workbook = new XSSFWorkbook(input);

			XSSFSheet sheet = workbook.getSheetAt(0);

			int count = sheet.getLastRowNum() - sheet.getFirstRowNum();

			for (int i = 0; i <= count; i++) {

				XSSFRow row = sheet.getRow(i);

				for (int j = 0; j < row.getLastCellNum(); j++) {

//			       

					XSSFCell cell = row.getCell(j);
					DataFormatter dft = new DataFormatter();

					String value = dft.formatCellValue(cell);
					System.out.print(value + " ");

				}

				System.out.println(" ");
			}

			workbook.close();

			input.close();
		} catch (FileNotFoundException e) {

			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}

}
